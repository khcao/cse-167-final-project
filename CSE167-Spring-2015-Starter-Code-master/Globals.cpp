#include "Globals.h"

Camera Globals::camera;

Light Globals::light;
Light Globals::directionalLight;
Light Globals::pointLight;
Light Globals::spotLight;

DrawData Globals::drawData;
UpdateData Globals::updateData;

//OBJObject Globals::bunny("bunny.obj");
//OBJObject Globals::dragon("dragon.obj");
//OBJObject Globals::bear("bear.obj");